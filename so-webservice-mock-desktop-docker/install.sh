docker stop webservice-mock-desktop
docker rm webservice-mock-desktop
docker rmi webservice-mock-desktop/mock

docker build -t webservice-mock-desktop .

docker run -d
    --name webservice-mock-desktop \
    -p 9080:8080 \
    -e SERVICE_ID=MY-MOCK
    -e MULTICAST_ENABLED=true
    -e MULTICAST_GROUP=224.0.0.3
    -e MULTICAST_PORT=50727
    webservice-mock-desktop

docker exec -t -i webservice-mock-desktop bash