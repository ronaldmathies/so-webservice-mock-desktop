FROM jboss/wildfly

MAINTAINER Ronald Mathies <ronald@sodeso.nl>

LABEL Version="1.0" Description="Installs the latest JBoss version along with the DeployKit tool"

ENV TERM=xterm

ENV WEBSERVICE_MOCK_HOME=${JBOSS_HOME}/deploykit

ADD scripts ${WEBSERVICE_MOCK_HOME}/bin/
ADD application ${WEBSERVICE_MOCK_HOME}/bin/application/
ADD config ${WEBSERVICE_MOCK_HOME}/bin/config

RUN ${JBOSS_HOME}/bin/add-user.sh admin admin --silent

USER root

RUN    chown -R jboss:jboss ${WEBSERVICE_MOCK_HOME} \
    && cd ${WEBSERVICE_MOCK_HOME}/bin \
    && chmod +x execute.sh \
    && chmod +x cli.sh \
    && yum -y install zip

USER jboss

# Start Wildfly and execute the CLI commands
# For some reason we need to delete the current folder otherwise Wildfly will crash upon boot.

RUN    cd ${WEBSERVICE_MOCK_HOME}/bin/config \
    && zip properties.jar *.properties \
    && cd ${WEBSERVICE_MOCK_HOME}/bin \
    && ./execute.sh \
    && rm -rf ${JBOSS_HOME}/standalone/configuration/standalone_xml_history/current

# Expose the HTTPS connector port.
EXPOSE 8443

# Expose the remote debugging port.
EXPOSE 8787

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0", "--debug", "8787"]