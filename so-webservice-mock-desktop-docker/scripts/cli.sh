#!/bin/bash

echo "Deploying module: MySql Driver"
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="module add --name=org.mysql --resources=./driver/mysql-connector-java-5.1.37.jar --dependencies=javax.api,javax.transaction.api,sun.jdk"

echo "Deploying module: Application resources"
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="module add --name=nl.deploykit.resources --resources=./config/properties.jar"

echo "Configuring: MySql driver."
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="/subsystem=datasources/jdbc-driver=mysql-driver:add(driver-name=mysql-driver, driver-module-name=org.mysql, driver-class-name=com.mysql.jdbc.Driver)"

echo "Configuring: Ddatasource."
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="data-source add --name=deploykit-ds --driver-name=mysql-driver --jndi-name=java:/jdbc/deploykit --connection-url=jdbc:mysql://$MYSQL_HOST:$MYSQL_PORT --new-connection-sql=\"use deploykit;\" --user-name=$MYSQL_USERNAME --password=$MYSQL_PASSWORD"

echo "Configuring: Security realm."
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="/subsystem=security/security-domain=deploykit-realm:add(cache-type=default)"

echo "Configuring: Authentication login-module."
# Configure the authentication login module.
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="/subsystem=security/security-domain=deploykit-realm/authentication=classic:add( \
        login-modules=[{ \
            code=org.jboss.security.auth.spi.DatabaseServerLoginModule, \
            flag=required, \
            module-options=>[ \
                dsJndiName=java:/jdbc/deploykit, \
                principalsQuery=\"SELECT t.password FROM t_user t WHERE t.username = ?\", \
                rolesQuery=\"SELECT r.label, '\''Roles'\'' FROM t_role r INNER JOIN t_role_user ru ON r.id = ru.role_id INNER JOIN t_user u ON ru.user_id = u.id WHERE u.username = ?\", \
            hashAlgorithm=SHA-256, \
            hashEncoding=base64, \
            ] \
        }] \
    )"

echo "Configuring: Authorization login-module."
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="/subsystem=security/security-domain=deploykit-realm/authorization=classic:add( \
        policy-modules=[{ \
            code=org.jboss.security.auth.spi.DatabaseServerLoginModule, \
            flag=required, \
            module-options=>[ \
                dsJndiName=java:/jdbc/deploykit, \
                principalsQuery=\"SELECT t.password FROM t_user t WHERE t.username = ?\", \
                rolesQuery=\"SELECT r.label, '\''Roles'\'' FROM t_role r INNER JOIN t_role_user ru ON r.id = ru.role_id INNER JOIN t_user u ON ru.user_id = u.id WHERE u.username = ?\", \
            hashAlgorithm=SHA-256, \
            hashEncoding=base64, \
            ] \
        }] \
    )"

echo "Reloading: Configuration."
${JBOSS_HOME}/bin/jboss-cli.sh -c --command="reload"


echo "Resolving application to deploy"
cd ${DEPLOYKIT_HOME}/bin/application

war=$(ls -t -U *.* | grep -m 1 dep-deploykit-desktop-web-*.war)
if [ -z ${war+x} ]; \
    then \
        echo "Application to deploy not found."; \
        exit -1; \
fi

echo "Deploying '$war'"

${JBOSS_HOME}/bin/jboss-cli.sh -c --command="deploy --force $war"

