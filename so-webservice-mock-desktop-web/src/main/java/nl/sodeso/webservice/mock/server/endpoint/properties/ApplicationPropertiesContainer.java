package nl.sodeso.webservice.mock.server.endpoint.properties;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = "application"
)
@FileResources(
    files = {
            @FileResource(
                    file= "/so-webservice-mock-desktop-configuration.properties",
                    monitor=false
            )
    }
)
public class ApplicationPropertiesContainer extends FileContainer {
}
