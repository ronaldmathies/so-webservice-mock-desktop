package nl.sodeso.webservice.mock.server.endpoint.properties;

import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractApplicationPropertiesEndpoint;
import nl.sodeso.gwt.ui.server.endpoint.properties.ApplicationPropertiesContainer;
import nl.sodeso.webservice.mock.client.Constants;
import nl.sodeso.webservice.mock.client.properties.WebServiceMockDesktopClientApplicationProperties;
import nl.sodeso.webservice.mock.server.listener.WebServiceMockDesktopServerApplicationProperties;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.webservicemockdesktop-properties"})
public class WebServiceMockDesktopPropertiesEndpoint extends AbstractApplicationPropertiesEndpoint<WebServiceMockDesktopClientApplicationProperties, WebServiceMockDesktopServerApplicationProperties> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<WebServiceMockDesktopClientApplicationProperties> getClientAppPropertiesClass() {
        return WebServiceMockDesktopClientApplicationProperties.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillApplicationProperties(WebServiceMockDesktopServerApplicationProperties serverAppProperties, WebServiceMockDesktopClientApplicationProperties clientAppProperties) {
        clientAppProperties.setName(Constants.MODULE_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WebServiceMockDesktopServerApplicationProperties getServerAppProperties() {
        return ApplicationPropertiesContainer.instance().getApplicationProperties();
    }

}

