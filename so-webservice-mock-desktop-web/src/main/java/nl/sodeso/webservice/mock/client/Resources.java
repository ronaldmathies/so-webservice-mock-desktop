package nl.sodeso.webservice.mock.client;

import com.google.gwt.core.client.GWT;

/**
 * The resources class can be used to register all instances created of Constants and Message interfaces for
 * i18n purposes.
 *
 * @author Ronald Mathies
 */
public final class Resources {

    private Resources() {}

    public final static ApplicationConstants APPLICATION_CONSTANTS = GWT.create(ApplicationConstants.class);

}