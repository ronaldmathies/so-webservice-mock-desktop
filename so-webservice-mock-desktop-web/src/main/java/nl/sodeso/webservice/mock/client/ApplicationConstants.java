package nl.sodeso.webservice.mock.client;

import com.google.gwt.i18n.client.Constants;

/**
 * The application constants should contain all the i18n resources that are globally for the application. It is
 * adviced to create new Constants for each domain object to keep the maintenance easy.
 *
 * @author Ronald Mathies
 */
public interface ApplicationConstants extends Constants {

    /**
     * The <code>DefaultStringValue</code> is used when no resource bundle could be found for the locale being used.
     *
     * @return The string value of the welcome constant.
     */
    @DefaultStringValue("Welcome")
    String welcome();

}