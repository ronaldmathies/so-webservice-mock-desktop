package nl.sodeso.webservice.mock.server.listener;

import nl.sodeso.commons.quartz.AbstractQuartzSchedulerContextListener;
import org.quartz.JobDataMap;

/**
 * @author Ronald Mathies
 */
public class WebServiceMockDesktopQuartzSchedulerContextListener extends AbstractQuartzSchedulerContextListener {

    @Override
    public JobDataMap getJobDataMap(String name) {
        return null;
    }
}
