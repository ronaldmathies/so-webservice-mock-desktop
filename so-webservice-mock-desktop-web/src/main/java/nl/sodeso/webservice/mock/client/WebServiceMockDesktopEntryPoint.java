package nl.sodeso.webservice.mock.client;

import com.google.gwt.core.client.GWT;
import nl.sodeso.gwt.ui.client.application.AvailableModulesService;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;
import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.webservice.mock.client.properties.WebServiceMockDesktopClientApplicationProperties;

import java.util.List;
import java.util.logging.Logger;

/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class WebServiceMockDesktopEntryPoint extends GwtApplicationEntryPoint<WebServiceMockDesktopClientApplicationProperties> {

    private static Logger logger = Logger.getLogger("Application.Boot");

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBeforeApplicationLoad() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ModuleEntryPoint> modules(){
        return ((AvailableModulesService)GWT.create(AvailableModulesService.class)).availableModules();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAfterApplicationLoad() {
         activateModule(nl.sodeso.webservice.mock.console.client.Constants.MODULE_ID);
    }
}
