package nl.sodeso.webservice.mock.server.listener;

import nl.sodeso.gwt.ui.server.endpoint.properties.ServerApplicationProperties;

/**
 * @author Ronald Mathies
 */
public class WebServiceMockDesktopServerApplicationProperties extends ServerApplicationProperties {

    public WebServiceMockDesktopServerApplicationProperties() {
        super();
    }
}
