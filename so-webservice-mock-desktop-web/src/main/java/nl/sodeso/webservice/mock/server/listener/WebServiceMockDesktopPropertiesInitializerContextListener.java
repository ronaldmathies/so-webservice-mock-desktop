package nl.sodeso.webservice.mock.server.listener;

import nl.sodeso.gwt.ui.server.listener.AbstractApplicationPropertiesInitializerContextListener;

/**
 * @author Ronald Mathies
 */
public class WebServiceMockDesktopPropertiesInitializerContextListener extends AbstractApplicationPropertiesInitializerContextListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getServerAppPropertiesClass() {
        return WebServiceMockDesktopServerApplicationProperties.class;
    }

}
