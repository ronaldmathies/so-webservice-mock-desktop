package nl.sodeso.webservice.mock.server.endpoint.session;

import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;

import javax.servlet.annotation.WebServlet;

/**
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.session"})
public class SessionEndpoint extends nl.sodeso.gwt.ui.server.endpoint.session.SessionEndpoint {

    @Override
    public void onBeforeLoggingIn() {
    }

    @Override
    public void onAfterLoggedIn(LoginResult loginResult) {
    }
}
